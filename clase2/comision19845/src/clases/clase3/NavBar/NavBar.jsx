// import {  , Navbar,  } from 'react-bootstrap'

import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Titulo from '../Titulo/Titulo'

function NavBar( {children} ) {
  console.log(children)
  return (
    <>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          {/* <Titulo /> */}
          <Container >
          <Navbar.Brand href="#home">React-Ecommerce</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="me-auto">
              <Nav.Link href="#features">Gorras</Nav.Link>
              <Nav.Link href="#pricing">Remeras</Nav.Link>            
              </Nav>
              <Nav>
              {/* <Nav.Link href="#deets">More deets</Nav.Link> */}
              <Nav.Link eventKey={2} href="#memes">
                  icono carrito
                  {/* <img className='w-25' src='https://w7.pngwing.com/pngs/225/984/png-transparent-computer-icons-shopping-cart-encapsulated-postscript-shopping-cart-angle-black-shopping.png' alt="esto es una imágen" /> */}
              </Nav.Link>
              </Nav>
          </Navbar.Collapse>
          </Container>
      </Navbar>
      { children }

    </>
  )
}

export default NavBar