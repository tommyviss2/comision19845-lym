import { useState } from 'react'
import NavBar from '../components/NavBar/NavBar'
import Titulo from '../components/Titulo/Titulo'
import ItemListContainer from '../container/ItemListContainer/ItemListContainer'

function RoutesApp() {
    const obj = { tit: 'Soy titulo de App', sutTit: 'soy sub tit' }  // estado

    return (
        <>
            <NavBar  />          
            <Titulo tituloProps='Titulo' subTit= 'subtitulo' />

            <ItemListContainer 
                greeting='Hola soy ItemListContainer' 
                titulo= { Titulo }
            />
        </>
    )
}

export default RoutesApp